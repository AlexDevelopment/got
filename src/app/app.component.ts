import { Component } from '@angular/core';
import { House } from './house';
import { GotService } from './got.service';

@Component({
  selector: 'my-app',
  templateUrl: './app.component.html',
  styleUrls: [ './app.component.css' ]
})
export class AppComponent  {
  houses: House[];

  constructor(private gotService: GotService) { }


  ngOnInit() {
    this.getHeroes();
  }

  getHeroes(): void {
    this.gotService.getHouses()
    .subscribe(houses => this.houses = houses);
  }
}
