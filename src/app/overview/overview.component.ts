import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { House } from '../house';
import { GotService } from '../got.service';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.css']
})
export class OverviewComponent implements OnInit {
  houses: House[];

  constructor(
    private gotService: GotService,
    private router: Router
    ){ }

  ngOnInit() {
    this.getHeroes();
  }

  navigate(data: House){
    this.router.navigate(['detail'], { queryParams:data})
  }
  
  getHeroes(): void {
   this.gotService.getHouses()
    .subscribe(houses => this.houses = houses);
  }
}