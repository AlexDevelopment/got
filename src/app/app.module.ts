import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { OverviewComponent } from './overview/overview.component';
import { DetailsComponent } from './details/details.component';

import { AppComponent } from './app.component';
import { HelloComponent } from './hello.component';
import { AppRoutingModule } from './app-routing.module';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { GotService } from './got.service';

@NgModule({
  imports: [ 
    BrowserModule, 
    FormsModule, 
    HttpClientModule,
    AppRoutingModule
  ],
  exports: [ RouterModule ],
  declarations: [ 
    AppComponent, 
    HelloComponent, 
    OverviewComponent,
    DetailsComponent ],
  
  bootstrap:    [ AppComponent ],
  
  // providers: [GotService]
})
export class AppModule { }
