import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GotService } from '../got.service';
import { House} from '../house';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {

  house: House;
  overlord: string;
  currentLord: string;
  founder: string; 
 

  constructor( 
    private route: ActivatedRoute,
    private gotService: GotService) { }

  ngOnInit() { 
    const name = this.route.queryParams.subscribe((data) => 
    { 
      this.house = data as House;
      
      this.gotService.getCharacter(data.overlord)
        .subscribe(overlord => this.overlord = overlord.name);

      this.gotService.getCharacter(data.currentLord)
        .subscribe(currentLord => this.currentLord = currentLord.name);

      this.gotService.getCharacter(data.founder)
        .subscribe(founder => this.founder = founder.name);
    })
  }
}