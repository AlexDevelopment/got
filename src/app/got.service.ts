import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable, of } from "rxjs";
import { catchError, map, tap } from "rxjs/operators";
import { House } from "./house";
import { Character } from "./character";

@Injectable({ providedIn: "root" })
export class GotService {
  httpOptions = {
    headers: new HttpHeaders({ "Content-Type": "application/json" })
  };

  constructor(private http: HttpClient) {}

  getHouses(): Observable<House[]> {
    return this.http.get<House[]>(
      "https://www.anapioficeandfire.com/api/houses"
    );
  }

  getCharacter(url: string): Observable<Character> {
    return this.http.get<Character>(url);
  }
}
